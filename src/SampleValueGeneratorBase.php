<?php

namespace Drupal\field_sample_value;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\ContextAwarePluginTrait;
use Drupal\Core\Plugin\PluginBase;

/**
 * Provides an interface for sample value generator plugins.
 */
abstract class SampleValueGeneratorBase extends PluginBase implements SampleValueGeneratorInterface {

  use ContextAwarePluginTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
    $this->pluginDefinition['context_definitions']['field_item_list'] = new ContextDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'prevent_save' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['prevent_save'] = [
      '#type' => 'checkbox',
      '#title' => t('Prevent save'),
      '#default_value' => $this->getConfiguration()['prevent_save'],
      '#description' => t('Prevents the entity from being saved if the sample value was not changed.'),
      '#weight' => 50,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldPreventSave(): bool {
    return (bool) $this->configuration['prevent_save'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // Nothing to do here.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // Nothing to do here.
  }

}
