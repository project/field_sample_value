<?php

namespace Drupal\field_sample_value;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\ContextAwarePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides an interface for sample value generator plugins.
 */
interface SampleValueGeneratorInterface extends PluginInspectionInterface, ConfigurableInterface, PluginFormInterface, ContextAwarePluginInterface {

  /**
   * Generates a sample value for the field items.
   *
   * @return void
   */
  public function generateSampleValue(FieldItemListInterface $item_list): void;

  /**
   * Whether an entity should not be saved with the generated sample value.
   *
   * @return bool
   */
  public function shouldPreventSave(): bool;

}
