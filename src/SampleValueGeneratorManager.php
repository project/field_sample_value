<?php

namespace Drupal\field_sample_value;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\field_sample_value\Annotation\SampleValueGenerator;

/**
 * Provides an SampleValueGeneratorManager plugin manager.
 */
class SampleValueGeneratorManager extends DefaultPluginManager {

  /**
   * Constructs a new SampleValueGeneratorManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Field/SampleValueGenerator', $namespaces, $module_handler, SampleValueGeneratorInterface::class, SampleValueGenerator::class);

    $this->setCacheBackend($cache_backend, 'sample_value_generators');
    $this->alterInfo('sample_value_generator_info');
  }

  /**
   * Returns an array of sample value generator plugin IDs for a field type.
   *
   * @param string $field_type
   *   The name of a field type.
   *
   * @return array
   *   An array of field sample value generator IDs.
   */
  public function getApplicableGenerators(string $field_type): array {
    $definitions = $this->getDefinitions();
    uasort($definitions, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
    $definitions = array_filter($definitions, function ($definition) use ($field_type) {
      return empty($definition['field_types']) || in_array($field_type, $definition['field_types'], TRUE);
    });

    return array_keys($definitions);
  }

}
