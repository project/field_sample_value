<?php

namespace Drupal\field_sample_value\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a SampleValueGenerator annotation object.
 *
 * Plugin Namespace: Plugin\Field\SampleValueGenerator.
 *
 * @see \Drupal\field_sample_value\SampleValueGeneratorInterface
 * @see \Drupal\field_sample_value\SampleValueGeneratorManager
 * @see plugin_api
 *
 * @Annotation
 */
class SampleValueGenerator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * An array of field types supported by the sample value generator.
   *
   * @var array
   */
  public $field_types = [];

  /**
   * A default weight for the plugin.
   *
   * This property is optional and it does not need to be declared.
   *
   * @var int
   */
  public $weight = 0;

}
