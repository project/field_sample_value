<?php

namespace Drupal\field_sample_value\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for a field with sample values.
 *
 * @Constraint(
 *   id = "SampleValue",
 *   label = @Translation("Sample value", context = "Validation"),
 * )
 */
class SampleValueConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public string $message = "The %name field contains automatically generated sample values.";

}
