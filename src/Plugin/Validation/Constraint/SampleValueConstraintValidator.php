<?php

namespace Drupal\field_sample_value\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the SampleValue constraint.
 */
class SampleValueConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    if ($items->isEmpty()) {
      return;
    }

    $entity = $items->getEntity();
    $field_name = $items->getName();
    if (!isset($entity->_sampleValues[$field_name])) {
      return;
    }

    if ($entity->get($field_name)->equals($entity->_sampleValues[$field_name])) {
      $this->context->addViolation($constraint->message, [
        '%name' => $items->getFieldDefinition()->getLabel(),
      ]);
    }
  }

}
