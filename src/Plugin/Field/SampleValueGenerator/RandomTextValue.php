<?php

namespace Drupal\field_sample_value\Plugin\Field\SampleValueGenerator;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field_sample_value\SampleValueGeneratorBase;
use Drupal\field_sample_value\SampleValueGeneratorInterface;

/**
 * Defines a random text sample value generator.
 *
 * @SampleValueGenerator(
 *   id = "random_text",
 *   label = @Translation("Random text"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   },
 *   weight = 0,
 * )
 */
class RandomTextValue extends SampleValueGeneratorBase implements SampleValueGeneratorInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'count' => 1,
      'filter_format' => filter_fallback_format(),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function generateSampleValue(FieldItemListInterface $item_list): void {
    $random = new Random();
    $settings = $item_list->getFieldDefinition()->getSettings();

    if (empty($settings['max_length'])) {
      // Textarea handling
      $value = $random->paragraphs($this->getConfiguration()['count']);
    }
    else {
      // Textfield handling.
      $value = substr($random->sentences(mt_rand(1, $this->getConfiguration()['count']), FALSE), 0, $settings['max_length']);
    }

    $values = [
      'value' => $value,
      'format' => $this->getConfiguration()['filter_format'],
    ];
    $item_list->setValue($values);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\Core\Field\FieldItemListInterface $field_item_list */
    $field_item_list = $this->getContextValue('field_item_list');
    $settings = $field_item_list->getFieldDefinition()->getSettings();

    if (empty($settings['max_length'])) {
      // Textarea handling
      $form['count'] = [
        '#type' => 'number',
        '#title' => $this->t('How many paragraphs would you like to generate?'),
        '#default_value' => $this->getConfiguration()['count'],
        '#size' => 2,
      ];

      $formats = [];
      foreach (filter_formats(\Drupal::currentUser()) as $id => $format) {
        $formats[$id] = $format->label();
      }
      $form['filter_format'] = [
        '#type' => 'select',
        '#title' => $this->t('Filter Format'),
        '#options' => $formats,
        '#default_value' => $this->getConfiguration()['filter_format'],
      ];
    }
    else {
      // Textfield handling.
      $form['count'] = [
        '#type' => 'number',
        '#title' => $this->t('How many sentences would you like to generate?'),
        '#default_value' => $this->getConfiguration()['count'],
        '#max' => ceil($settings['max_length'] / 3),
        '#size' => 2,
      ];
    }

    return $form;
  }

}
