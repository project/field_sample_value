<?php

namespace Drupal\field_sample_value\Plugin\Field\SampleValueGenerator;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\field_sample_value\SampleValueGeneratorBase;
use Drupal\field_sample_value\SampleValueGeneratorInterface;

/**
 * Defines the default sample value generator.
 *
 * @SampleValueGenerator(
 *   id = "default",
 *   label = @Translation("Field type default"),
 *   weight = -10,
 * )
 */
class DefaultValue extends SampleValueGeneratorBase implements SampleValueGeneratorInterface {

  /**
   * {@inheritdoc}
   */
  public function generateSampleValue(FieldItemListInterface $item_list): void {
    $item_list->generateSampleItems();
  }

}
