<?php

namespace Drupal\field_sample_value;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Default implementation of the sample value entity generator.
 */
class SampleValueEntityGenerator implements SampleValueEntityGeneratorInterface {

  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected readonly SampleValueGeneratorManager $sampleValueGeneratorManager
  ) {}

  /**
   * {@inheritdoc}
   */
  public function createWithSampleValues(string $entity_type_id, string $bundle = NULL, array $values = []): FieldableEntityInterface {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

    if ($bundle_key = $entity_type->getKey('bundle')) {
      if (!$bundle) {
        throw new EntityStorageException("No entity bundle was specified");
      }
      if (!array_key_exists($bundle, $this->entityTypeBundleInfo->getBundleInfo($entity_type_id))) {
        throw new EntityStorageException(sprintf("Missing entity bundle. The \"%s\" bundle does not exist", $bundle));
      }
      $values[$bundle_key] = $bundle;
    }

    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $this->entityTypeManager->getStorage($entity_type_id)->create($values);
    $this->populateWithSampleValues($entity);

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function populateWithSampleValues(FieldableEntityInterface $entity): void {
    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      if (!_field_sample_value_field_definition_has_generator_configured($field_definition)) {
        continue;
      }

      /** @var \Drupal\field_sample_value\SampleValueGeneratorInterface $generator */
      $sample_value_info = $field_definition->getThirdPartySettings('field_sample_value');
      $generator = $this->sampleValueGeneratorManager->createInstance($sample_value_info['id'], $sample_value_info['configuration'] ?? []);
      $generator->generateSampleValue($entity->get($field_name));

      // Keep track the generated sample value if needed.
      if ($generator->shouldPreventSave()) {
        $entity->_sampleValues[$field_name] = clone $entity->get($field_name);
      }
    }
  }

   /**
    * {@inheritdoc}
    */
  public function populateWithSampleValue(FieldItemListInterface $field):void {
    $field_definition = $field->getFieldDefinition();
    if (_field_sample_value_field_definition_has_generator_configured($field_definition)) {
      // Use the configured generator.
      /** @var \Drupal\field_sample_value\SampleValueGeneratorInterface $generator */
      $sample_value_info = $field_definition->getThirdPartySettings('field_sample_value');
      $generator = $this->sampleValueGeneratorManager->createInstance($sample_value_info['id'], $sample_value_info['configuration'] ?? []);
    }
    else {
      // Pick one to use. Anything is better than Core's generator.
      $generator_ids = $this->sampleValueGeneratorManager->getApplicableGenerators($field_definition->getType());
      $generator = $this->sampleValueGeneratorManager->createInstance(array_pop($generator_ids), []);
    }
    $generator->generateSampleValue($field);
  }

}
