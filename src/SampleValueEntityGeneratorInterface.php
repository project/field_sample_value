<?php

namespace Drupal\field_sample_value;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Provides an interface for the sample value entity generator service.
 */
interface SampleValueEntityGeneratorInterface  {

  /**
   * Creates an entity with sample field values.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   * @param array $values
   *   (optional) Any default values to use during generation.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   A fieldable content entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the bundle does not exist or was needed but not specified.
   */
  public function createWithSampleValues(string $entity_type_id, string $bundle = NULL, array $values = []): FieldableEntityInterface;

  /**
   * Populates an entity with sample field values.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *    A fieldable content entity.
   */
  public function populateWithSampleValues(FieldableEntityInterface $entity): void;

  /**
   * Populate a field with a sample value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   A field.
   *
   * @return void
   */
  public function populateWithSampleValue(FieldItemListInterface $field): void;

}
