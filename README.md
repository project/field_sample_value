# Field Sample Value

This module makes the field sample generator pluggable. You can configure a generator for a field by navigating Structure > Content Types > Manage Fields > Edit the Field.
Then check "Set sample value" and select a generator. If no generator is selected for a field the last option in the list will be selected. If the field requires a text editor
the global default can be configured by running `drush cset filter.settings fallback_format YOUR_FILTER_FORMAT_ID`.
